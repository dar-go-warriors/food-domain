package food_domain

import "time"

type Menu struct {
	Id       string    `json:"id"`
	Date     time.Time `json:"date"`
	Dishes   []Dish    `json:"dishes"`
	IsActive bool      `json:"is_active"`
}

type Dish struct {
	Name   string `json:"name"`
	MenuId string `json:"menu_id"`
	Number int    `json:"number"`
}
