package food_domain

import "time"

type Order struct {
	User
	Id        string    `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	Dishes    []Dish    `json:"dishes"`
	Status    string    `json:"status"` //pending OR completed
}

type User struct {
	Name        string `json:"name"`
	PhoneNumber string `json:"phone_number"`
}
